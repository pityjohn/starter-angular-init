// importer les modules en commencant par ceux d'angular
// puis classer par ordre alphabetique
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ItemsService } from './core/services/items/items.service';
import { HomeModule } from './home/home.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from '../environments/environment.prod';


@NgModule({
  imports: [ BrowserModule, NgbModule.forRoot(), CoreModule, SharedModule,
             AppRoutingModule, HomeModule, PageNotFoundModule,
             AngularFireModule.initializeApp(environment.firebase), AngularFirestoreModule,
            ],
  declarations: [ AppComponent, ],
  providers: [ ItemsService ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    if (!environment.production) {
       console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
  }
}
