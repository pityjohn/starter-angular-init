import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListItemsComponent } from './containers/list-items/list-items.component';
import { ItemRoutingModule } from './item-routing.module';
import { ItemComponent } from './components/item/item.component';
import { SharedModule } from '../shared/shared.module';
import { AddItemComponent } from './containers/add-item/add-item.component';
import { EditComponent } from './containers/edit/edit.component';

@NgModule({
  imports: [ CommonModule, ItemRoutingModule, SharedModule, ],
  declarations: [ListItemsComponent, ItemComponent, AddItemComponent, EditComponent],
  exports: [ListItemsComponent]
})
export class ItemsModule { }
