import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/models/item.model';
import { ItemsService } from '../../../core/services/items/items.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
  }

  addItem(itemA: Item): void {
//    console.log(itemA);
    this.itemsService.add(itemA);
  }

}
