import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../../../core/services/items/items.service';
import { Item } from '../../../shared/models/item.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
  }

  updateItem(itemE: Item): void {
        this.itemsService.update(itemE);
      }

    }
