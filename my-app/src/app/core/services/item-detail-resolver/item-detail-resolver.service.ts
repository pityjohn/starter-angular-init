import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Item } from '../../../shared/models/item.model';
import { ItemsService } from '../items/items.service';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemDetailResolverService implements Resolve<Item> {
 
  constructor(
    private itemsService: ItemsService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) 
  {
    const id = route.paramMap.get('id');
    return this.itemsService.getItem(id).take(1).map(data => 
      {if (data) 
        {// console.log(`item form resolver : ${this.item.name}`);
        return data;
        } 
      else 
        { // if not found
        this.router.navigate(['/items']);
        return null;
        }
      }
    );
  }
}