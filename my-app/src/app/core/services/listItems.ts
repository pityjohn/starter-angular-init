import { Item } from '../../shared/models/item.model';

export const ITEMS: Item[] =[
  {
  id: 'wxcvb',
  name: 'weiyi',
  reference: '1234',
  state: 'A livrer'
  },
  {
  id: 'azerty',
  name: 'gabriel',
  reference: '2345',
  state: 'En cours'
  },
  {
  id: 'qsdfg',
  name: 'stéphane',
  reference: '3456',
  state: 'Livrée'
  }
];
