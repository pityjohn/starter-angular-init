import { Injectable } from '@angular/core';
import { ITEMS } from '../listItems';
import { Item } from '../../../shared/models/item.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { ItemsModule } from '../../../items/items.module';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ItemsService {
  private itemsCollection: AngularFirestoreCollection<Item>;
  collection$: Observable<Item[]>;
  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Item>('collection'); // 'collection' car nom donnée a la base
    this.setCollection(this.itemsCollection.valueChanges());
   }

  getCollection(): Observable<Item[]> {
    return this.collection$;
  }
  setCollection(collection: Observable<Item[]>) {
    this.collection$ = collection;
  }
  add(itemAd: Item): void {
    console.log('add' + itemAd);
  }
  update(itemAd: Item): void {
    console.log('update' + itemAd);
  }

  getItem(id: string): Observable<Item> {
    const item = this.afs.doc<Item>(`collection/${id}`).valueChanges();
    return item;
  }
}
