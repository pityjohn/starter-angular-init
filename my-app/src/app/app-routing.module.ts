import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

// import { HomeComponent } from './home/components/home/home.component';
// import { ListItemsComponent } from './items/containers/list-items/list-items.component';
// import { PageNotFoundComponent } from './page-not-found/components/page-not-found/page-not-found.component';

// Prit du site angular.io
const appRoutes: Routes = [
  { path: 'items', loadChildren: 'app/items/items.module#ItemsModule'},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( appRoutes, {preloadingStrategy: PreloadAllModules}, ) // { enableTracing: true } )
    ],
  declarations: [],
  exports: [RouterModule],
})
export class AppRoutingModule { }
