import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { State } from '../../enum/state.enum';
import { Item } from '../../models/item.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  form: FormGroup;
  state = State;
  stateEnum = Object.values(State);
  @Output() itemO: EventEmitter<Item>= new EventEmitter();
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void{
  this.form = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
    reference: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    state: [State.ALIVRER]
  });
  }

  process(): void{
//    console.log(this.form.value);
    this.itemO.emit(this.form.value);
  }

  isError(champ: string){
    return this.form.get(champ).dirty && this.form.get(champ).hasError('minlength');
  }
}
