import { Directive, Input, OnInit, HostBinding, } from '@angular/core';
import { State } from '../enum/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit {
  @Input('appState') appState: State;
  @HostBinding('class') elementClass: string;
  constructor() {}

  ngOnInit(): void {
    this.elementClass = this.formatClass(this.appState);
  }
  private removeAccent(state: string): string {
    return state.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }
  private formatClass(state: string): string {
    return `state-${(this.removeAccent(state)).toLowerCase().replace(' ', '-')}`;
  }
}
