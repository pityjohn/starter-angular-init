import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NavigationComponent } from './components/navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { StateDirective } from './directives/state.directive';
import { FormComponent } from './components/form/form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [ CommonModule, NgbModule, RouterModule, ReactiveFormsModule],
  declarations: [NavigationComponent, StateDirective, FormComponent],
  exports: [NavigationComponent, StateDirective, FormComponent]
})
export class SharedModule { }
