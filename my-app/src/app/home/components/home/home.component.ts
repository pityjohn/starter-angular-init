import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: string;
  constructor() { }
  ngOnInit() {
    this.title = 'Bienvenue sur ma page de formation angular 5 - 05/02/2018';
  }

}
